<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/en_travaux.git

return [

	// E
	'entravaux_description' => 'À activer avant une phase de maintenance, ce plugin affiche aussitôt une page d’avertissement, {{à la place}} du site public. Attention : seul le webmestre accède encore à l’espace privé et au site public.',
	'entravaux_nom' => 'En travaux',
	'entravaux_slogan' => 'Indiquer une phase de maintenance',
];
