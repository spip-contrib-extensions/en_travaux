<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-entravaux?lang_cible=eo
// ** ne pas modifier le fichier **

return [

	// E
	'entravaux_description' => 'Tiu kromprogramo, aktivigota antaŭ bontenado, montras tuj avertilon, {{anstataŭ}} la publikan retejon. Atentu : nur la retejestro povas eniri la privatan spacon kaj vidi la publikan retejon.',
	'entravaux_nom' => 'Prilaborata retejo',
	'entravaux_slogan' => 'Signali momenton de nedisponebleco kaj bontenado',
];
