<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-entravaux?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// E
	'entravaux_description' => 'Ak ju aktivujete pred fázou údržby, tento zásuvný modul {{namiesto}} verejne prístupnej stránky okamžite zobrazí stránku s upozornením. Pozor: Iba webmaster sa môže dostať do súkromnej zóny a na verejne prístupnú stránku.',
	'entravaux_nom' => 'Rekonštruuje sa',
	'entravaux_slogan' => 'Nastaviť stránku rekonštrukcie',
];
