<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-entravaux?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// E
	'entravaux_description' => 'Displays a defined page during maintenance period on all your public pages.',
	'entravaux_nom' => 'Under construction',
	'entravaux_slogan' => 'Indicate a maintenance phase',
];
