<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/entravaux-193?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// A
	'activer_message' => 'Attivare una sessione di manutenzione',
	'autoriser_travaux' => 'Di che stato ho bisogno per vedere il sito durante i lavori?',

	// E
	'en_travaux' => 'In manutenzione',
	'erreur_droit' => 'Devi essere Webmaster per poter usare il plugin "In Manutenzione"!',

	// I
	'info_disallow_robot' => 'Il tuo sito è protetto contro i robot ed i motori di ricerca durante la manutenzione.',
	'info_maintenance_en_cours' => 'Il sito è in manutenzione! Solo i Webmaster del sito possono vedere il sito pubblico ed accedere all’area privata.',
	'info_message' => 'Questa pagina consente di visualizzare un messaggio temporaneo su tutte le pagine del sito durante una sessione di manutenzione.',
	'info_travaux_texte' => 'Questo sito è in manutenzione, torna più tardi...',

	// L
	'label_disallow_robots' => 'Proteggi il sito da robot e motori di ricerca',

	// M
	'message_temporaire' => 'Il vostro messaggio di manutenzione:',

	// P
	'parametrage_page_travaux' => 'Configurazione della pagina di manutenzione',

	// T
	'texte_lien_publier' => 'Rendi pubblico il sito',
];
