<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/entravaux-193?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// A
	'activer_message' => 'Het onderhoud activeren',
	'autoriser_travaux' => 'Wat is de benodigde status om de site tijdens onderhoud te bekijken?',

	// E
	'en_travaux' => 'Werk in uitvoering',
	'erreur_droit' => 'Je moet webmaster zijn om deze plugin te kunnen gebruiken!',

	// I
	'info_disallow_robot' => 'Tijdens onderhoud is je website beschermd tegen robots en zoekmachines.',
	'info_maintenance_en_cours' => 'De site is in onderhoud! Alleen de webmaster heeft toegang tot de publieke site en het privé-gedeelte.',
	'info_message' => 'Je kunt onderhoud van de site activeren en op alle bladzijdes een tijdelijke waarschuwingsboodschap weergeven.',
	'info_travaux_texte' => 'Deze site is momenteel in onderhoud.
_ Kom later terug.',

	// L
	'label_disallow_robots' => 'Bescherm de site tegen robots en zoekmachines',

	// M
	'message_temporaire' => 'Je tijdelijke boodschap:',

	// P
	'parametrage_page_travaux' => 'Instelling van de tijdelijke badzijde',

	// T
	'texte_lien_publier' => 'Maak de site weer toegankelijk',
];
