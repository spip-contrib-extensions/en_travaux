<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-entravaux?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// E
	'entravaux_description' => 'Consente di visualizzare un messaggio personalizzabile durante una sessione di manutenzione su tutte le pagine del sito pubblico.',
	'entravaux_nom' => 'In manutenzione',
	'entravaux_slogan' => 'Indica una fase di manutenzione',
];
