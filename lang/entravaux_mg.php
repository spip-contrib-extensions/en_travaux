<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/entravaux-193?lang_cible=mg
// ** ne pas modifier le fichier **

return [

	// A
	'activer_message' => 'Activer la maintenance',
	'autoriser_travaux' => 'Quel est le statut  nécessaire pour voir le site durant les travaux ?',

	// E
	'en_travaux' => 'En travaux',
	'erreur_droit' => 'Vous devez être Webmestre pour utiliser le plugin En Travaux !',

	// I
	'info_disallow_robot' => 'Votre site est protégé contre les robots et moteur de recherche pendant la maintenance.',
	'info_maintenance_en_cours' => 'Le site est en maintenance ! Seuls les webmestres du site peuvent voir le site public et accèder à l’espace privé.',
	'info_message' => 'Vous pouvez activer la maintenance du site et configurer un message temporaire sur toute les pages du site pendant une phase de maintenance.',
	'info_travaux_texte' => 'Ce site est actuellement en travaux.
_ Revenez plus tard.',

	// L
	'label_disallow_robots' => 'Protéger le site contre les robots et moteur de recherche',

	// M
	'message_temporaire' => 'Votre message temporaire :',

	// P
	'parametrage_page_travaux' => 'Paramétrage de la page temporaire',

	// T
	'texte_lien_publier' => 'Rendre le site public',
];
