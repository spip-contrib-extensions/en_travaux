<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-entravaux?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// E
	'entravaux_description' => 'لتنشيط قبل مرحلة الصيانة، وهذا البرنامج المساعد يعرض فورا صفحة تحذير، {{بدلا من}} الموقع العام. تحذير: لا يزال مشرف الموقع فقط يصل إلى المنطقة الخاصة والموقع العام.',
	'entravaux_nom' => 'في صيانة الموقع',
	'entravaux_slogan' => 'الدخول في مرحلة الصيانة',
];
